import React, { Component } from 'react';
import './App.css';
import {Container, Button, Alert} from 'react-bootstrap';
import ProductList from './ProductList';
// import RoutingExample from './RoutingExample';


class App extends Component {
  render() {
  return (
    <div className="App">
    <Container>
      <h1 style={{textAlign:"center"}}> React Tutorial</h1> 
      <ProductList />
    </Container>
    </div>
  );
 }
}


export default App;